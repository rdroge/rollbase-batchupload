
const fs = require("fs");
const csv = require("fast-csv");
const request = require("request");
const config = require("config");
const rbConfig = config.get('rbConfig');
const Client = require('node-rest-client').Client;

/* Required in case intermediate certificates were not serviced correctly by the web server

var rootCas = require('ssl-root-cas/latest').create();

require('https').globalAgent.options.ca = rootCas;

rootCas
.addFile(certConfig.certificate1)
.addFile(certConfig.certificate2)
.addFile(certConfig.certificate3);

*/

const fileLocation = rbConfig.csv_file_location;  
const sessionId = '';

var login_args = {
    headers: {  loginName: rb_user,
                password: rb_pw,
                "Content-Type": "application/json" 
    },
}

var parser = null;
var stream = fs.createReadStream(fileLocation);
var client = new Client(); 


var rb_url  = rbConfig.rollbase_uri + "/rest/api"; 
var rb_user = rbConfig.rollbase_user; 
var rb_pw   = rbConfig.rollbase_password; 
var rb_fieldName = rbConfig.rollbase_field; 

var login_args = {
    headers: {  loginName: rb_user,
                password: rb_pw,
                "Content-Type": "application/json" 
    }
}

var p1 = new Promise((resolve, reject) => {
       client.get(rb_url + '/login', login_args, function (data, response) {
            resolve(data.resp.sessionId);
       });
       

});
p1.then((sessionId) => {


    var parser = csv.parseFile(fileLocation)
    .on("data", function(data){
        parser.pause();
        getFromRollbase(data, sessionId, parser);
       
    }).on("end", function(){
     //When ready uploading everything let the user know and logout the user  
     console.log("done downloading...logging out!");
     var logout_args = {
        headers: {  sessionId: sessionId,
                    output: "json" 
        }
    }
    setTimeout(function() { client.get(rb_url + '/logout', logout_args, function (data, response) {
        // parsed response body as js object 
        // console.log(data, response);
    }); }, 4000);
 });


});

function getFromRollbase(data, sessionId, parser) {
    
    var fileName = data[1].replace(/^.*[\\\/]/, '');

    var req = request({
        method: "GET",
        preambleCRLF: true,
        postambleCRLF: true,
        uri: rb_url + '/getBinaryData',
        headers: {
            'content-type': 'multi-part/form-data'
        },
        qs: {
            id: data[0],
            fieldName: rb_fieldName,
            fileName: fileName,
            sessionId: sessionId,
            contentType: 'application/octet-stream',
            output: 'json'
        },
    },function(error, response, body) {
            if (error) {
                return console.error('download failed: ', error);
            }
            var jsonBody = JSON.parse(body);
            var fileData = jsonBody.file.fileData;
            fs.writeFileSync(fileName, fileData, "base64");	
            parser.resume();
        }
    );
    
}


