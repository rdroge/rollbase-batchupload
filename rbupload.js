var fs = require("fs");
var csv = require("fast-csv");
var request = require("request");
const rbConfig = config.get('rbConfig');
const certConfig = config.get('certConfig');
var Client = require('node-rest-client').Client;

/* Required in case intermediate certificates were not serviced correctly by the web server

var rootCas = require('ssl-root-cas/latest').create();

require('https').globalAgent.options.ca = rootCas;

rootCas
.addFile(certConfig.certificate1)
.addFile(certConfig.certificate2)
.addFile(certConfig.certificate3);

*/

var fileLocation = rbConfig.csv_file_location;  

var client = new Client(); 

var rb_url  = rbConfig.rollbase_uri + "/rest/api"; 
var rb_user = rbConfig.rollbase_user; 
var rb_pw   = rbConfig.rollbase_password; 
var rb_fieldName = rbConfig.rollbase_field; 

var sessionId = '';

var login_args = {
    headers: {  loginName: rb_user,
                password: rb_pw,
                "Content-Type": "application/json" 
    }
}

var p1 = new Promise((resolve, reject) => {

        client.get(rb_url + '/login', login_args, function (data, response) {
            resolve(data.resp.sessionId);
        });
       

});
p1.then((sessionId) => {
    
csv
 .fromStream(stream)
 .on("data", function(data){
     filePath = data[1];
     var bitmap = fs.readFileSync(filePath);
     
     // convert binary data to base64 encoded string
     data.push(new Buffer(bitmap).toString('base64'));
     sendtoRollbase(data, sessionId, file);
    
 })
 
 .on("end", function(){
     console.log("CSV file has been parsed! Now sending files to Rollbase...");
 });
 

});

function sendtoRollbase(data, sessionId, file) {
    //var rb_upload = new Promise((resolve, reject) => {
    var fileName = data[1].replace(/^.*[\\\/]/, '');
    var req = request({
        method: "POST",
        preambleCRLF: true,
        postambleCRLF: true,
        uri: rb_url + '/setBinaryData',
        headers: {
            'content-type': 'multi-part/form-data'
        },
        qs: {
            id: data[0],
            fieldName: rb_fieldName,
            fileName: fileName,
            sessionId: sessionId,
            contentType: 'application/pdf'
        },
    },
    function(error, response, body) {
            console.log("blaat");
            if (error) {
                return console.error('upload failed: ', error);
            } else {
            console.log('Upload successfull!: ', body);
            }
        }
    );
    var form = req.form();
    form.append('value', fs.readFileSync(data[1]).toString('base64'));	
}
