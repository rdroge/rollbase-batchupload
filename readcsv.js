var fs = require("fs");
var csv = require("fast-csv");
var request = require("request");
var Client = require('node-rest-client').Client;
//This program expects a comma delimited CSV file
//First column should contain record ID in Rollbase, second column the absolut path to the file that needs to be imported
var fileLocation = '<FILE_PATH>/<FILE_NAME>';  
var parser = null;
var stream = fs.createReadStream(fileLocation);
var client = new Client(); 

/* Productieomgeving SKISS */
var rb_url  = "<RB_URL>/rest/api"; //REST API URL for Rollbase
var rb_user = "<RB_USERNAME>"; //Rollbase Username
var rb_pw   = "<RB_PASSWORD>"; //Rollbase Password
var rb_fieldName = '<RB_FIELD_INTEGRATION_NAME>'; //Integration name of the document field in Rollbase

var sessionId = '';

var login_args = {
    headers: {  loginName: rb_user,
                password: rb_pw,
                "Content-Type": "application/json" 
    }
}


var p1 = new Promise((resolve, reject) => {
       client.post(rb_url + '/login', login_args, function (data, response) {
            // parsed response body as js object 
            resolve(data.resp.sessionId);
       });
       

});
p1.then((sessionId) => {

    var parser = csv.fromStream(stream)
    .on("data", function(data){
        filePath = data[1];
        var bitmap = fs.readFileSync(filePath);
        
        // convert binary data to base64 encoded string
        data.push(new Buffer(bitmap).toString('base64'));
        parser.pause();
        sendtoRollbase(data, sessionId, parser);
       
    }).on("end", function(){
     //When ready uploading everything let the user know and logout the user  
     console.log("done uploading...logging out!");
     var logout_args = {
        headers: {  sessionId: sessionId,
                    output: "json" 
        }
    }
    setTimeout(function() { client.get(rb_url + '/logout', logout_args, function (data, response) {
        // parsed response body as js object 
        console.log(data, response);
    }); }, 4000);
 });


});

function sendtoRollbase(data, sessionId, parser) {
    
    var fileName = data[1].replace(/^.*[\\\/]/, '');
    console.log("Handling file: ", fileName);
    var req = request({
        method: "POST",
        preambleCRLF: true,
        postambleCRLF: true,
        uri: rb_url + '/setBinaryData',
        headers: {
            'content-type': 'multi-part/form-data'
        },
        qs: {
            id: data[0],
            fieldName: rb_fieldName,
            fileName: fileName,
            sessionId: sessionId,
            contentType: 'application/pdf'
        },
    },function(error, response, body) {
            if (error) {
                return console.error('upload failed: ', error);
            }
            console.log('Upload successfull!: ', body);
            parser.resume();
        }
    );
    var form = req.form();
    form.append('value', fs.readFileSync(data[1]).toString('base64'));	
}


    

        
