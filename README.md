# Rollbase / InfiniteBlue in General

[Progress Rollbase / InfiniteBlue](https://infiniteblue.com/) is a model-based RAD-platform (Rapid Application Development) that allows users to build and deploy SAAS-based applications. 

## Using the batch upload / download programs

These programs (written in NodeJS) make use of the [Rollbase/InfiniteBlue REST API](https://documentation.infiniteblue.com/#platform/Platform%20REST%20Methods.htm%3FTocPath%3DAPI%2520References%7CPlatform%2520REST%2520Methods%7C_____0) to download / upload binary files from Rollbase/InfiniteBlue.

**To upload documents**
> node rbupload.js

**To download document**
> node rbdownload.js

### CSV files ### 

**Bulkupload**

Requires a CSV-file (comma delimited) that contains the existing Rollbase object ID's (first column) and the file location and name (second column) of the file that you want to upload to Rollbase and add to that object.

**Bulkdownload**

Requires a CSV-file (comma delimited) that contains the existing Rollbase object ID's (first column) and the file name (second column) of the file that you want to download from the specified Rollbase object ID.

### Configuration (config/default.json) ###

**Rollbase Configuration**

*csv_file_location*<br>
File location and file name of your CSV-file

*rollbase_uri*<br>
The URI endpoint to your Rollbase/InfiniteBlue installation

*rollbase_user*<br>
Your Rollbase/InfiniteBlue username (with sufficient permissions to use the REST API)

*rollbase_pw*<br>
Your Rollbase/InfiniteBlue password

*rollbase_field*<br>
The integration name of the field (within the object definition) that contains the binary data/documents

**Certificate Configuration (optional)**

*certificate1, certificate2, certificate3*<br>
If you require intermediate certificates that were not serviced by the webserver itself, you can add them (name and location)                                  here  